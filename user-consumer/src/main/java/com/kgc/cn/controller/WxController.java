package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.third.WxService;
import com.kgc.cn.common.service.user.OrderService;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import com.kgc.cn.webservice.UserWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;


/**
 * Created by boot on 2019/12/2.
 */
@Api(tags = "wx管理")
@Controller
@RequestMapping(value = "wx")
public class WxController {
    @Autowired
    private UserWebService userWebService;
    @Reference
    private WxService wxService;
    @Reference
    private OrderService orderService;

    @Autowired
    RedisUtils redisUtils;

    @ApiOperation("wx登录")
    @PostMapping("/toLogin")
    @ResponseBody
    public ReturnResult toLogin(HttpServletRequest request) throws UnsupportedEncodingException {
        String sessionId = request.getSession().getId();
        String redirect = null;
        try {
            redirect = wxService.toLogin(sessionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //返回uri与sessionId
        return ReturnResultUtils.returnSuccess(redirect, sessionId);
    }

    @GetMapping("/loginCallBack")
    @ResponseBody
    public ReturnResult callBack(String code, String state, HttpServletRequest request) {
        //第二步通过第一部的code换取accessToken与openId
        String num = userWebService.wxLogin(code, request, state);
        //请求get
        if ("10003".equals(num)) {
            return ReturnResultUtils.returnSuccess(ReturnEnum.UserEnum.LOGINSUCCESS);
        }
        if ("10004".equals(num)) {
            return ReturnResultUtils.returnSuccess(ReturnEnum.UserEnum.OPENID);
        }
        return ReturnResultUtils.returnSuccess(ReturnEnum.UserEnum.MsgErr);
    }


    @PostMapping("/payCallBack")
    @ResponseBody
    public void payCallBack(HttpServletRequest request, HttpServletResponse response) throws Exception {
        InputStream inputStream = request.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        String line = "";
        StringBuffer sbf = new StringBuffer();
        while ((line = bufferedReader.readLine()) != null) {
            sbf.append(line);
        }
        Map<String, String> params = wxService.xmlToMap(sbf.toString());
        if ("SUCCESS".equals(params.get("return_code"))) {
            String orderId = params.get("out_trade_no");
            if (redisUtils.hasKey(NameSpaceEnum.VIP.getNamespace() + orderId)) {
                String userId = (String) redisUtils.get(NameSpaceEnum.VIP.getNamespace() + orderId);
                UserInfoVo userInfoVo = userWebService.select(UserInfoVo.builder().userId(userId).build()).get(0);
                userWebService.openVip(orderId, 1, userInfoVo);
            } else {
                orderService.changeState(orderId, 20002);
            }

        }


        if (wxService.isSignatureValid(sbf.toString())) {
            response.setContentType("text/xml; charset=utf-8");
            response.getWriter().write("<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
        }
    }
}

