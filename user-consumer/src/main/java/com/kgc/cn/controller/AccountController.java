package com.kgc.cn.controller;

import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.webservice.AccountService;
import com.kgc.cn.webservice.UserWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by boot on 2019/12/25
 */
@RestController
@RequestMapping(value = "/account")
@Api(tags = "账户管理")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private UserWebService userWebService;

    @PostMapping(value = "/SendSms")
    @ApiOperation("发信息")
    @LoginRequired
    public ReturnResult SendSms(@ApiParam("手机号") @RequestParam String phone, @CurrentUser @ApiIgnore UserInfoVo userInfoVo) {
        accountService.SendSms(phone, userInfoVo);
        return ReturnResultUtils.returnSuccess();
    }

    @PostMapping(value = "/SendEmail")
    @ApiOperation("发邮箱")
    @LoginRequired
    public ReturnResult SendEmail(@ApiParam("邮箱") @RequestParam String email, @CurrentUser @ApiIgnore UserInfoVo userInfoVo) {
        accountService.SendEmail(email, userInfoVo);
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 绑定手机
     *
     * @param phone      手机号
     * @param code       验证码
     * @param userInfoVo 当前用户
     * @return 统一返回
     */
    @PostMapping(value = "/boundPhone")
    @ApiOperation("绑定手机")
    @LoginRequired
    public ReturnResult boundPhone(@ApiParam(value = "需要绑定的手机号", required = true) @RequestParam String phone,
                                   @ApiParam(value = "验证码", required = true) @RequestParam String code,
                                   @CurrentUser @ApiIgnore UserInfoVo userInfoVo, HttpServletRequest request) {
        int state = accountService.boundPhone(phone, code, userInfoVo);


        if (state == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.CodeEnum.CODE_ERROR);
        }
        if (state == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.PhoneEunm.REPEAT);
        }
        if (state == 4) {
            return ReturnResultUtils.returnFail(ReturnEnum.UserEnum.MsgErr);
        }

        userWebService.relogin(userInfoVo.getUserId(), request);
        return ReturnResultUtils.returnSuccess();

    }

    /**
     * 绑定邮箱
     *
     * @param email      邮箱
     * @param code       验证码
     * @param userInfoVo 当前用户
     * @return 统一返回
     */
    @PostMapping(value = "/boundEmail")
    @ApiOperation("绑定邮箱")
    @LoginRequired
    public ReturnResult boundEmail(@ApiParam(value = "需要绑定的邮箱", required = true) @RequestParam String email,
                                   @ApiParam(value = "验证码", required = true) @RequestParam String code,
                                   @CurrentUser @ApiIgnore UserInfoVo userInfoVo, HttpServletRequest request) {
        int state = accountService.boundEmail(email, code, userInfoVo);


        if (state == 2) {
            return ReturnResultUtils.returnFail(ReturnEnum.CodeEnum.CODE_ERROR);
        }
        if (state == 3) {
            return ReturnResultUtils.returnFail(ReturnEnum.PhoneEunm.REPEAT);
        }
        if (state == 4) {
            return ReturnResultUtils.returnFail(ReturnEnum.UserEnum.MsgErr);
        }
        userWebService.relogin(userInfoVo.getUserId(), request);
        return ReturnResultUtils.returnSuccess();

    }

    /**
     * 绑定微信
     *
     * @param openId 微信openId
     * @param token  sessionId
     * @return 统一返回
     */
    @ApiOperation("绑定微信")
    @GetMapping(value = "/boundWx")
    public String boundWx(String openId, String token, HttpServletRequest request) {
        return accountService.boundWx(openId, token);
    }
}
