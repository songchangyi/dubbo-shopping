package com.kgc.cn.config.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)  //作用于
@Retention(RetentionPolicy.RUNTIME) // 作用时间
public @interface CurrentUser {
}
