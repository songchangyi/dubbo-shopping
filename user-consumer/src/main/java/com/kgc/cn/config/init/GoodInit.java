package com.kgc.cn.config.init;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.NameSpaceEnum;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.service.management.ProductService;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by scy on 2019/12/4
 */

@Component
public class GoodInit implements ApplicationRunner {

    @Autowired
    RedisUtils redisUtils;
    @Reference
    ProductService productService;

    @Override
    public void run(ApplicationArguments args) {
        List<ProductInfoVo> productInfoVoList = productService.show(new ProductInfoParam());
        if (!CollectionUtils.isEmpty(productInfoVoList)) {
            productInfoVoList.forEach(infoVo -> {
                redisUtils.set(NameSpaceEnum.PRODUCT_AMOUNT.getNamespace() + infoVo.getProductId(), infoVo.getProductStock());
            });
        }

    }
}
