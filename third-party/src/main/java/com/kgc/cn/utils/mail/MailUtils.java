package com.kgc.cn.utils.mail;

import com.kgc.cn.common.model.param.mail.MailParam;
import freemarker.template.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * Created by scy on 2019/12/9
 */

@Component
public class MailUtils {

    @Autowired
    JavaMailSender mailSender;


    @Value("${spring.mail.username}")
    private String from;

    /**
     * 发送普通文本邮件
     *
     * @param mailParam 邮件参数
     */
    public void sendSimpleMail(MailParam mailParam) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(mailParam.getTo());//收信人
        message.setSubject(mailParam.getSubject());//主题
        message.setText(mailParam.getContent());//内容
        message.setFrom(from);//发信人
        mailSender.send(message);
    }

    /**
     * 发送HTML邮件
     *
     * @param mailParam 邮件参数
     */
    public void sendHtmlMail(MailParam mailParam) {
        //使用MimeMessage，MIME协议
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        //MimeMessageHelper帮助我们设置更丰富的内容
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(mailParam.getTo());
            helper.setSubject(mailParam.getSubject());
            helper.setText(mailParam.getContent(), true);//true代表支持html
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送带附件的邮件
     *
     * @param mailParam 邮件参数
     */
    public void sendAttachmentMail(MailParam mailParam) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            //true代表支持多组件，如附件，图片等
            helper.setFrom(from);
            helper.setTo(mailParam.getTo());
            helper.setSubject(mailParam.getSubject());
            helper.setText(mailParam.getContent(), true);
            //添加附件，可多次调用该方法添加多个附件
            for (String filePath : mailParam.getFilePaths()) {
                FileSystemResource file = new FileSystemResource(new File(filePath));
                String fileName = file.getFilename();
                helper.addAttachment(fileName, file);
            }
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 发送带图片的邮件
     *
     * @param mailParam 邮件参数
     */
    public void sendInlineResourceMail(MailParam mailParam) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(mailParam.getTo());
            helper.setSubject(mailParam.getSubject());
            helper.setText(mailParam.getContent(), true);
            mailParam.getPictures().forEach((k, v) -> {
                FileSystemResource res = new FileSystemResource(new File(v));
                try {
                    helper.addInline(k, res);//重复使用添加多个图片
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            });

            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }


    public void sendTemplateMail(MailParam mailParam, String templateName) throws Exception {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setFrom(from);
        helper.setTo(mailParam.getTo());

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_20);
        configuration.setClassForTemplateLoading(this.getClass(), "/templates");

        String html = FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(templateName), mailParam.getTemplateParams());

        helper.setSubject(mailParam.getSubject());
        helper.setText(html, true);//重点，默认为false，显示原始html代码，无效果

        mailSender.send(mimeMessage);
    }


}