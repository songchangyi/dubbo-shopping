package com.kgc.cn.utils.url;

import com.kgc.cn.utils.OrderParam;
import com.kgc.cn.utils.WxPayParam;
import com.kgc.cn.utils.xmlUrl.WxPayXmlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by scy on 2019/12/10
 */

@Component
public class WxPayUrl {
    @Autowired
    private WxPayParam wxPayParam;

    /**
     * 微信支付
     *
     * @param order 订单详情
     * @return 二维码
     * @throws Exception
     */
    public String wxPay(OrderParam order) throws Exception {
        SortedMap<String, String> data = new TreeMap();
        //公众账号ID
        data.put("appid", wxPayParam.getAppid());
        //商户号
        data.put("mch_id", wxPayParam.getMch_id());
        //随机字符串
        data.put("nonce_str", WxPayXmlUtil.generateUUID(32));
        //商品描述
        data.put("body", order.getName());
        //商户订单号
        data.put("out_trade_no", order.getId());
        //标价金额
        data.put("total_fee", String.valueOf(order.getPrice()));
        //终端IP
        data.put("spbill_create_ip", wxPayParam.getCreate_ip());
        //通知地址
        data.put("notify_url", wxPayParam.getNotify_url());
        //交易类型
        data.put("trade_type", wxPayParam.getTrade_type());
        //签名
        data.put("sign", WxPayXmlUtil.generateSignature(data, wxPayParam.getKey()));

        String orderXml = WxPayXmlUtil.mapToXml(data);
        return Url.doPost(wxPayParam.getUrl(), orderXml, 1000);

    }
}
