package com.kgc.cn.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by scy on 2019/12/10
 */

@Data
@Component
@ConfigurationProperties(prefix = "wxPay")
public class WxPayParam {
    private String appid;
    private String mch_id;
    private String notify_url;
    private String key;
    private String trade_type;
    private String url;
    private String create_ip;
}
