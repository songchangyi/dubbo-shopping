package com.kgc.cn.consumer.webService;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.param.ProductUpdateParam;
import com.kgc.cn.common.model.vo.ProductDiscountVo;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.consumer.param.excel.DiscountExcelParam;
import com.kgc.cn.consumer.param.excel.ProductInfoExcelParam;

import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */


public interface ProductWebService {

    /**
     * 添加商品
     *
     * @param list 商品信息集合
     */
    void addProductInfo(List<ProductInfoExcelParam> list);

    /**
     * 修改商品
     *
     * @param productUpdateParam 商品参数
     */
    void updateProductInfo(ProductUpdateParam productUpdateParam);

    /**
     * 查询商品（模糊查询）
     *
     * @param productInfoParam 商品参数
     * @return 商品信息
     */
    List<ProductInfoVo> show(ProductInfoParam productInfoParam);

    /**
     * 删除商品
     *
     * @param productId 商品id
     * @return 影响条数
     */
    int deleteProduct(String productId);

    /**
     * 添加折扣（单件）
     *
     * @param discountVo 折扣信息
     * @return 影响条数
     */
    boolean discount(ProductDiscountVo discountVo);

    /**
     * 批量折扣（excel）
     *
     * @param params 折扣信息集合
     */
    void discountByExcel(List<DiscountExcelParam> params);

    /**
     * 商品折扣查询
     *
     * @return 商品信息
     */
    List<ProductDiscountsParam> queryProductDiscount();

    /**
     * 商品分页
     *
     * @param productInfoParam 商品参数
     * @param page             当前页码
     * @param size             每页规格
     * @return 分页信息
     */
    PageInfo<ProductInfoVo> paging(ProductInfoParam productInfoParam, int page, int size);

    /**
     * 商品数量查询
     *
     * @return 商品数量
     */
    List<Map<String, Map<String, Integer>>> queryCount();

    /**
     * 查询商品
     *
     * @return
     */
    List<Map<String, Map<String, List<ProductInfo>>>> merchandise();
}
