package com.kgc.cn.consumer.param.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.util.Date;

/**
 * Created by scy on 2019/12/12
 */

@Data
public class EmployeeExcelParam extends BaseRowModel {
    @ExcelProperty(value = "姓名", index = 0)
    private String employeeName;
    @ExcelProperty(value = "性别", index = 1)
    private String employeeGender;
    @ExcelProperty(value = "职位", index = 2)
    private Integer roleId;
    @ExcelProperty(value = "部门", index = 3)
    private String departmentId;
    @ExcelProperty(value = "邮箱", index = 4)
    private String employeeEmail;
    @ExcelProperty(value = "手机", index = 5)
    private String employeePhone;
    @ExcelProperty(value = "入职时间", index = 6)
    private Date entryTime;
}
