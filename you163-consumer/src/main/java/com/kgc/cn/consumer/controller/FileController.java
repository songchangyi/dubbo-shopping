package com.kgc.cn.consumer.controller;

import com.kgc.cn.common.utils.ReturnResult;
import com.kgc.cn.common.utils.result.ReturnResultUtils;
import com.kgc.cn.consumer.config.annotation.PowerRequired;
import com.kgc.cn.consumer.webService.FileWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by scy on 2019/12/16
 */

@Api(tags = "文件管理")
@RestController
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    FileWebService fileWebService;


    /**
     * 员工信息导出
     *
     * @param path 文件导出路径
     * @return 成功
     */
    @ApiOperation("员工信息导出")
    @GetMapping(value = "/checkOutEmployee")
    @PowerRequired(departmentId = "3003")
    public ReturnResult checkOutEmployee(@ApiParam(value = "文件导出路径", required = true) @RequestParam String path) {
        fileWebService.checkOutEmployee(path);
        return ReturnResultUtils.returnSuccess();
    }

    /**
     * 商品信息导出
     *
     * @param path 文件导出路径
     * @return 成功
     */
    @ApiOperation("商品信息导出")
    @GetMapping(value = "/checkOutProduct")
    @PowerRequired(departmentId = "3002")
    public ReturnResult checkOutProduct(@ApiParam(value = "文件导出路径", required = true) @RequestParam String path) {
        fileWebService.checkOutProduct(path);
        return ReturnResultUtils.returnSuccess();
    }


}
