package com.kgc.cn.consumer.config.scheduled;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by scy on 2019/12/17
 */

@Configuration
@EnableScheduling
public class ScheduledConfig {
}
