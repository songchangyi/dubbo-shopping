package com.kgc.cn.consumer.webService.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.param.EmployeeCheckOut;
import com.kgc.cn.common.model.param.ProductCheckOut;
import com.kgc.cn.common.service.management.EmployeeService;
import com.kgc.cn.common.service.management.ProductService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.common.utils.time.TimeUtils;
import com.kgc.cn.consumer.param.excel.EmployeeCheckOutParam;
import com.kgc.cn.consumer.param.excel.ProductCheckOutParam;
import com.kgc.cn.consumer.util.excel.EasyExcelUtils;
import com.kgc.cn.consumer.webService.FileWebService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by scy on 2019/12/16
 */

@Service
public class FileWebServiceImpl implements FileWebService {

    @Reference
    EmployeeService employeeService;

    @Reference
    ProductService productService;


    @Override
    public void checkOutEmployee(String path) {
        if (!path.contains(".")) {
            if ("/".equals(String.valueOf(path.charAt(path.length() - 1)))) {
                path += "employeeInfo" + TimeUtils.getCurrentTime() + ".xlsx";
            } else {
                path += "/employeeInfo" + TimeUtils.getCurrentTime() + ".xlsx";
            }
        }
        List<EmployeeCheckOut> checkOutList = employeeService.checkOut();
        List<EmployeeCheckOutParam> params = Lists.newArrayList();
        checkOutList.forEach(out -> {
            EmployeeCheckOutParam param = CopyUtils.copy(out, EmployeeCheckOutParam.class);
            params.add(param);
        });
        EasyExcelUtils.writeExcelByModel(path, "员工信息", params, EmployeeCheckOutParam.class);
    }

    @Override
    public void checkOutProduct(String path) {
        if (!path.contains(".")) {
            if ("/".equals(String.valueOf(path.charAt(path.length() - 1)))) {
                path += "productInfo" + TimeUtils.getCurrentTime() + ".xlsx";
            } else {
                path += "/productInfo" + TimeUtils.getCurrentTime() + ".xlsx";
            }
        }
        List<ProductCheckOut> checkOutList = productService.checkOut();
        List<ProductCheckOutParam> params = Lists.newArrayList();
        checkOutList.forEach(out -> {
            ProductCheckOutParam param = CopyUtils.copy(out, ProductCheckOutParam.class);
            params.add(param);
        });
        EasyExcelUtils.writeExcelByModel(path, "商品信息", params, ProductCheckOutParam.class);
    }


}
