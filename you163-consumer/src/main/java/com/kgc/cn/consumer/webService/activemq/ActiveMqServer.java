package com.kgc.cn.consumer.webService.activemq;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.service.management.EmployeeService;
import com.kgc.cn.common.service.management.PowerService;
import com.kgc.cn.common.utils.encrypt.EncryptUtil;
import com.kgc.cn.consumer.param.mail.MailParam;
import com.kgc.cn.consumer.util.active.ActiveMqUtils;
import com.kgc.cn.consumer.util.password.PasswordUtils;
import com.kgc.cn.consumer.webService.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/12
 */

@Component
public class ActiveMqServer {

    @Autowired
    private ActiveMqUtils activeMqUtils;

    @Autowired
    private MailService mailService;

    @Reference
    private EmployeeService employeeService;

    @Reference
    private PowerService powerService;

    /**
     * 发送入职邮件
     *
     * @param infos 员工信息集合
     */
    public void sendAccountMail(List<EmployeeInfoVo> infos) {
        infos.forEach(info -> {
            String password = PasswordUtils.createPassword();
            Map<String, Object> params = Maps.newHashMap();
            params.put("username", info.getEmployeeName());
            params.put("phone", info.getEmployeePhone());
            params.put("password", password);
            MailParam mailParam = MailParam.builder().to(info.getEmployeeEmail()).subject("入职通知").templateParams(params).build();
            activeMqUtils.sendMsgByQueue("accountMail", mailParam);
            EmployeeAccount employeeAccount = EmployeeAccount.builder().account(info.getEmployeePhone())
                    .employeeId(info.getEmployeeId()).password(password)
                    .level(powerService.showRoleLevel(info.getRoleId().toString())).build();
            activeMqUtils.sendMsgByQueue("addAccount", employeeAccount);
        });


    }

    /**
     * 发送密码重置邮件
     *
     * @param info    目标员工信息
     * @param current 修改者信息
     */
    public void sendResetPasswordMail(EmployeeInfoVo info, EmployeeInfoVo current) {
        String password = PasswordUtils.createPassword();
        Map<String, Object> params = Maps.newHashMap();
        params.put("username", info.getEmployeeName());
        params.put("phone", info.getEmployeePhone());
        params.put("password", password);
        params.put("mender", current.getEmployeeName());
        MailParam mailParam = MailParam.builder().to(info.getEmployeeEmail()).subject("密码重置通知").templateParams(params).build();
        activeMqUtils.sendMsgByQueue("resetPasswordMail", mailParam);
        EmployeeAccount employeeAccount = EmployeeAccount.builder().employeeId(info.getEmployeeId())
                .password(EncryptUtil.getInstance().Base64Encode(password)).build();
        activeMqUtils.sendMsgByQueue("resetPassword", employeeAccount);


    }

    /**
     * 入职邮件监听
     *
     * @param mailParam 邮件参数
     */
    @JmsListener(destination = "accountMail")
    private void listener1(MailParam mailParam) {
        mailService.sendAccountMails(mailParam);
    }

    /**
     * 账号监听
     *
     * @param employeeAccount 员工账号
     */
    @JmsListener(destination = "addAccount")
    private void listener2(EmployeeAccount employeeAccount) {
        employeeService.addEmployeeAccount(employeeAccount);
    }

    /**
     * 密码重置邮件监听
     *
     * @param mailParam 邮件参数
     */
    @JmsListener(destination = "resetPasswordMail")
    private void listener3(MailParam mailParam) {
        mailService.sendResetPasswordMail(mailParam);
    }

    /**
     * 密码重置监听
     *
     * @param employeeAccount 员工账号
     */
    @JmsListener(destination = "resetPassword")
    private void listener4(EmployeeAccount employeeAccount) {
        employeeService.updateEmployeeAccount(employeeAccount);
    }
}
