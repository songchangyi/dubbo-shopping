package com.kgc.cn.consumer.webService.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.param.ProductUpdateParam;
import com.kgc.cn.common.model.vo.ProductDiscountVo;
import com.kgc.cn.common.model.vo.ProductInfoVo;
import com.kgc.cn.common.service.management.ProductService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.consumer.param.excel.DiscountExcelParam;
import com.kgc.cn.consumer.param.excel.ProductInfoExcelParam;
import com.kgc.cn.consumer.webService.ProductWebService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */

@Service
public class ProductWebServiceImpl implements ProductWebService {
    @Reference
    private ProductService productService;

    @Override
    public void addProductInfo(List<ProductInfoExcelParam> list) {
        list.forEach(param -> {
            ProductInfoVo productInfoVo = CopyUtils.copy(param, ProductInfoVo.class).createId();
            productService.addProductInfo(productInfoVo);
        });
    }

    @Override
    public void updateProductInfo(ProductUpdateParam productUpdateParam) {
        ProductInfoVo productInfoVo = CopyUtils.copy(productUpdateParam, ProductInfoVo.class);
        productService.updateProduct(productInfoVo);
    }

    @Override
    public List<ProductInfoVo> show(ProductInfoParam productInfoParam) {
        return productService.show(productInfoParam);
    }

    @Override
    public int deleteProduct(String productId) {
        return productService.deleteProduct(productId);
    }

    @Override
    public boolean discount(ProductDiscountVo discountVo) {
        return productService.discount(discountVo) == 1;
    }

    @Override
    public void discountByExcel(List<DiscountExcelParam> params) {
        params.forEach(param -> {
            productService.discount(CopyUtils.copy(param, ProductDiscountVo.class));
        });
    }

    @Override
    public List<ProductDiscountsParam> queryProductDiscount() {
        return productService.queryProductDiscount();
    }

    @Override
    public PageInfo<ProductInfoVo> paging(ProductInfoParam productInfoParam, int page, int size) {
        return productService.paging(productInfoParam, page, size);
    }

    @Override
    public List<Map<String, Map<String, Integer>>> queryCount() {
        return productService.queryCount();
    }

    @Override
    public List<Map<String, Map<String, List<ProductInfo>>>> merchandise() {
        return productService.merchandise();
    }
}
