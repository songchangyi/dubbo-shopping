package com.kgc.cn.consumer.param.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by scy on 2019/12/16
 */

@Data
public class EmployeeCheckOutParam extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = -8577116147263520220L;
    @ExcelProperty(value = "工号", index = 0)
    private String employeeId;
    @ExcelProperty(value = "姓名", index = 1)
    private String employeeName;
    @ExcelProperty(value = "性别", index = 2)
    private String employeeGender;
    @ExcelProperty(value = "职位", index = 3)
    private String role;
    @ExcelProperty(value = "部门", index = 4)
    private String department;
    @ExcelProperty(value = "邮箱", index = 5)
    private String employeeEmail;
    @ExcelProperty(value = "手机", index = 6)
    private String employeePhone;
    @ExcelProperty(value = "入职时间", index = 7)
    private Date entryTime;
    @ExcelProperty(value = "是否离职", index = 8)
    private String isDimission;
    @ExcelProperty(value = "离职时间", index = 9)
    private Date dimissionTime;
}
