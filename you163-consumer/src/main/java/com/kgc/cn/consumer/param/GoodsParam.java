package com.kgc.cn.consumer.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by boot on 2019/12/3.
 */
@ApiModel
@Data
public class GoodsParam implements Serializable {
    private static final long serialVersionUID = 2790671524891574618L;
    @ApiModelProperty("商品id")
    private String gId;
    @ApiModelProperty("商品名")
    private String gName;
    @ApiModelProperty(value = "商品价格", example = "1")
    private Double gPrice;
    @ApiModelProperty("商品类型")
    private String gProperty;
}
