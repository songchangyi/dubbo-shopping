package com.kgc.cn.consumer.webService.impl;

import com.kgc.cn.consumer.param.mail.MailParam;
import com.kgc.cn.consumer.util.mail.MailUtils;
import com.kgc.cn.consumer.webService.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    MailUtils mailUtils;

    @Override
    @Async
    public void sendAccountMails(MailParam mailParam) {

        try {
            mailUtils.sendTemplateMail(mailParam, "account.ftl");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void sendResetPasswordMail(MailParam mailParam) {
        try {
            mailUtils.sendTemplateMail(mailParam, "resetPassword.ftl");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
