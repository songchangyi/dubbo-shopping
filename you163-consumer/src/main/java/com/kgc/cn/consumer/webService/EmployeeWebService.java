package com.kgc.cn.consumer.webService;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.param.AttendanceParam;
import com.kgc.cn.common.model.param.EmployeeInfoParam;
import com.kgc.cn.common.model.param.EmployeeParam;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.consumer.param.excel.EmployeeExcelParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by scy on 2019/12/10
 */


public interface EmployeeWebService {

    /**
     * 录入员工
     *
     * @param list excel数据
     */
    void addEmployee(List<EmployeeExcelParam> list);

    /**
     * 员工登录
     *
     * @param phone    手机号
     * @param password 密码
     * @param request  网页请求
     * @return 员工信息
     */
    String toLogin(String phone, String password, HttpServletRequest request);


    /**
     * 注销登录
     *
     * @param request sessionId
     * @return 布尔类型
     */
    Boolean loginOut(HttpServletRequest request);


    /**
     * 员工信息修改
     *
     * @param employeeInfoParam 员工信息
     * @param current           当前用户信息
     * @return 影响条数
     */
    int updateEmployeeInfo(EmployeeInfoParam employeeInfoParam, EmployeeInfoVo current);


    /**
     * 删除员工
     *
     * @param employeeId 员工编号
     * @param current    当前用户信息
     * @return 影响条数
     */
    int delEmployee(String employeeId, EmployeeInfoVo current);


    /**
     * 密码重置
     *
     * @param employeeId id
     * @return 影响条数
     */
    int resetPassword(String employeeId, EmployeeInfoVo current);


    /**
     * 打卡
     *
     * @param request sessionId
     * @param current 当前用户信息
     * @return 影响条数
     */
    int addEmployeeAttendance(HttpServletRequest request, EmployeeInfoVo current);


    /**
     * 查询出勤
     *
     * @param flag true 已出勤 false 未出勤
     * @return 出勤或未出勤信息
     */
    PageInfo<AttendanceParam> queryAttendance(boolean flag, int page, int size);

    /**
     * 员工分页
     *
     * @param employeeParam 员工参数
     * @param page          当前页码
     * @param size          分页规格
     * @return 分页信息
     */
    PageInfo<EmployeeInfoVo> paging(EmployeeParam employeeParam, int page, int size);


}
