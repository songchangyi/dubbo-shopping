package com.kgc.cn.consumer.param.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by scy on 2019/12/16
 */

@Data
public class ProductCheckOutParam extends BaseRowModel implements Serializable {
    @ExcelProperty(value = "商品id", index = 0)
    private String productId;
    @ExcelProperty(value = "商品名", index = 1)
    private String productName;
    @ExcelProperty(value = "价格", index = 2)
    private BigDecimal productPrice;
    @ExcelProperty(value = "库存", index = 3)
    private Integer productStock;
    @ExcelProperty(value = "类别", index = 4)
    private String category;
    @ExcelProperty(value = "入库时间", index = 5)
    private Date createTime;
    @ExcelProperty(value = "修改时间", index = 6)
    private Date updateTime;
}
