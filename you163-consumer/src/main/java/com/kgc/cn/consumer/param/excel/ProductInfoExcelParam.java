package com.kgc.cn.consumer.param.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by boot on 2019/12/12
 */
@Data
public class ProductInfoExcelParam extends BaseRowModel {
    @ExcelProperty(value = "商品名称", index = 0)
    private String productName;
    @ExcelProperty(value = "单价", index = 1)
    private BigDecimal productPrice;
    @ExcelProperty(value = "库存", index = 2)
    private Integer productStock;
    @ExcelProperty(value = "类别", index = 3)
    private String categoryId;
}
