INSERT INTO product_category (
	id,
	category_id,
	category_name
)
VALUES
	(1, 1001, '鞋服'),
	(2, 1002, '食品'),
	(3, 1003, '电子'),
	(4, 1004, '家居'),
	(5, 1005, '化妆');

INSERT INTO employee_role
(id,role_id,role_name,role_level)
VALUES
(1	,2001	,'经理',1),
(2	,2002	,'部门主管',2),
(3	,2003	,'员工',3);



INSERT INTO employee_department (
	id,
	department_id,
	department_name
)
VALUES
	(1, 3001, '市场部'),
	(2, 3002, '后勤部'),
	(3, 3003, '人事部');

-- 市场部  查询
-- 后勤部  商品录入
-- 人事部  人事管理
-- 经理 拥有所有权限
-- 部门主管 可以拥有本部门所有权限
-- 员工 只能查和增（本部门）
