package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by boot on 2019/12/13
 */
@ApiModel("商品修改")
@Data
public class ProductInfoParam implements Serializable {
    private static final long serialVersionUID = 4331668994858363793L;
    @ApiModelProperty("商品id")
    private String productId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty(value = "商品价格", example = "0.00")
    private BigDecimal productPrice;
    @ApiModelProperty(value = "商品件数", example = "123")
    private Integer productStock;
    @ApiModelProperty("商品类别")
    private String categoryId;

    @ApiModelProperty("创建时间-开始")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeStart;

    @ApiModelProperty("创建时间-结束")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeEnd;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("修改时间-开始")
    private Date updateTimeStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("修改时间-结束")
    private Date updateTimeEnd;

}
