package com.kgc.cn.common.utils.exception.exception;


import com.kgc.cn.common.utils.exception.enums.ExceptionEnum;
import com.kgc.cn.common.utils.exception.enums.ResultCode;
import lombok.Data;


@Data
public class BusinessException extends RuntimeException {

    private int code;
    private String message;
    private Object data;

    public BusinessException() {
        ExceptionEnum exceptionEnum = ExceptionEnum.getByEClass(this.getClass());
        if (exceptionEnum != null) {
            code = exceptionEnum.getResultCode().getCode();
            message = exceptionEnum.getResultCode().getMessage();
        }

    }

    public BusinessException(String message) {
        this();
        this.message = message;
    }

//    public BusinessException(String format, Object... objects) {
//        this();
//        format = StringUtils.replace(format, "{}", "%s");
//        this.message = String.format(format, objects);
//    }
//
//    public BusinessException(String msg, Throwable cause, Object... objects) {
//        this();
//        String format = StringUtils.replace(msg, "{}", "%s");
//        this.message= String.format(format, objects);
//    }

    public BusinessException(ResultCode resultCode, Object data) {
        this(resultCode);
        this.data = data;
    }

    public BusinessException(ResultCode resultCode) {
        code = resultCode.getCode();
        message = resultCode.getMessage();
    }
}
