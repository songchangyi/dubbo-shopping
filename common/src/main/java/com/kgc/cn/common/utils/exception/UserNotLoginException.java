package com.kgc.cn.common.utils.exception;

import com.kgc.cn.common.utils.exception.exception.BusinessException;

public class UserNotLoginException extends BusinessException {
    public UserNotLoginException() {
        super();
    }

    public UserNotLoginException(Object data) {
        super();
        super.setData(data);
    }
}
