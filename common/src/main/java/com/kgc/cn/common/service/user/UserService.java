package com.kgc.cn.common.service.user;

import com.kgc.cn.common.model.dto.VipRecord;
import com.kgc.cn.common.model.vo.UserInfoVo;

import java.util.List;

/**
 * Created by scy on 2019/12/23
 */
public interface UserService {
    /**
     * 用户注册
     *
     * @param userInfoVo
     * @return
     */
    int registered(UserInfoVo userInfoVo);

    /**
     * 搜索
     *
     * @param userInfoVo
     * @return
     */
    List<UserInfoVo> select(UserInfoVo userInfoVo);

    /**
     * 根据手机号登录
     *
     * @param userPhone
     * @param userPassword
     * @return
     */

    UserInfoVo phoneLogin(String userPhone, String userPassword);

    /**
     * 根据邮箱登录
     *
     * @param userEmail
     * @param userPassword
     * @return
     */

    UserInfoVo mailLogin(String userEmail, String userPassword);

    int addVipRecord(VipRecord record);

    int updateUser(UserInfoVo userInfoVo);

}
