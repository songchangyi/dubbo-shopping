package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by xulei on 2019/12/12
 */
@ApiModel(value = "员工查询信息")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeParam implements Serializable {

    private static final long serialVersionUID = -7579808627546447068L;


    @ApiModelProperty(value = "员工编号")
    private String employeeId;
    @ApiModelProperty(value = "员工姓名")
    private String employeeName;
    @ApiModelProperty(value = "员工性别")
    private String employeeGender;
    @ApiModelProperty(value = "职位编号", example = "1")
    private Integer roleId;
    @ApiModelProperty(value = "部门编号")
    private String departmentId;
    @ApiModelProperty(value = "员工邮箱")
    private String employeeEmail;
    @ApiModelProperty(value = "员工手机号")
    private String employeePhone;
    @ApiModelProperty(value = "在职状态", example = "1")
    private Integer isDimission;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "入职日期开始")
    private Date entryTimeStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "入职日期结束")
    private Date entryTimeEnd;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "离职日期开始")
    private Date dimissionTimeStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "离职日期结束")
    private Date dimissionTimeEnd;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间开始")
    private Date updateTimeStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间结束")
    private Date updateTimeEnd;


}
