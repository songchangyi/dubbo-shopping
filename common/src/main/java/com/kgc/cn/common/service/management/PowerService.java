package com.kgc.cn.common.service.management;

/**
 * Created by scy on 2019/12/13
 */


public interface PowerService {

    /**
     * 获取职位权限等级
     *
     * @param roleId 职位id
     * @return 权限等级
     */
    String showRoleLevel(String roleId);

    /**
     * 获取账号对应权限
     *
     * @param employeeId 工号
     * @return 权限等级
     */
    String showAccountLevel(String employeeId);

}
