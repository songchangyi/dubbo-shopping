package com.kgc.cn.common.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("账号信息")
public class EmployeeAccountVo implements Serializable {
    private static final long serialVersionUID = 881872197700997852L;
    @ApiModelProperty("id")
    private String employeeId;
    @ApiModelProperty("账号")
    private String account;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("权限等级")
    private String level;


}