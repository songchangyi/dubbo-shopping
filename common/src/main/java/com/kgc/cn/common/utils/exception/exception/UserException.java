package com.kgc.cn.common.utils.exception.exception;


import com.kgc.cn.common.utils.exception.ParameterInvalidException;

public class UserException extends RuntimeException {


    public static void main(String[] args) {
        try {
            int i = 2 / 0;
        } catch (Exception e) {
            System.out.println(1);
            throw new ParameterInvalidException();
        }
    }
}
