package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -6267385713208960648L;
    private String userId;

    private String nickname;

    private String userPhone;

    private String categoryId;

    private String userOpenid;

    private String userEmail;

    private String userPassword;

    private Integer userAge;

    private Integer userSex;

    private Integer deleteFlag;

    public UserInfo createId() {
        userId = UUID.randomUUID().toString().substring(0, 8);
        return this;
    }

}