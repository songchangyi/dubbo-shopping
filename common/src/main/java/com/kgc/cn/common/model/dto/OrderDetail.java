package com.kgc.cn.common.model.dto;

import com.kgc.cn.common.utils.time.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail implements Serializable {
    private static final long serialVersionUID = -2293855194426077134L;
    private String orderId;

    private String userId;

    private String productId;

    private Integer productNum;

    private Integer orderState;

    private Date createTime;

    private Date updateTime;

    public OrderDetail createId() {
        orderId = TimeUtils.getOrderTime() + UUID.randomUUID().toString().substring(0, 4);

        return this;
    }

}