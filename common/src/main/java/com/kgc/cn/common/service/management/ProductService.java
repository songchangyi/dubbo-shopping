package com.kgc.cn.common.service.management;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ProductInfo;
import com.kgc.cn.common.model.param.ProductCheckOut;
import com.kgc.cn.common.model.param.ProductDetailsParam;
import com.kgc.cn.common.model.param.ProductDiscountsParam;
import com.kgc.cn.common.model.param.ProductInfoParam;
import com.kgc.cn.common.model.vo.ProductDiscountVo;
import com.kgc.cn.common.model.vo.ProductInfoVo;

import java.util.List;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */


public interface ProductService {


    /**
     * 添加商品（excel）
     *
     * @param productInfoVo 商品信息vo
     * @return 录入结果
     */
    int addProductInfo(ProductInfoVo productInfoVo);

    /**
     * 修改商品
     *
     * @param productInfoVo 商品信息vo
     * @return 影响条数
     */
    int updateProduct(ProductInfoVo productInfoVo);

    /**
     * 查询商品（模糊查询）
     *
     * @param productInfoParam 商品信息参数
     * @return 商品信息
     */
    List<ProductInfoVo> show(ProductInfoParam productInfoParam);

    /**
     * 商品检出（excel）
     *
     * @return 检查结果
     */
    List<ProductCheckOut> checkOut();

    /**
     * 删除商品
     *
     * @param productId 商品id
     * @return 影响条数
     */
    int deleteProduct(String productId);

    /**
     * 添加折扣（单件）
     *
     * @param discountVo 折扣vo
     * @return 影响条数
     */
    int discount(ProductDiscountVo discountVo);

    /**
     * 商品分页
     *
     * @param productInfoParam 商品信息
     * @param page             当前页码
     * @param size             每页规格
     * @return 分页信息
     */
    PageInfo<ProductInfoVo> paging(ProductInfoParam productInfoParam, int page, int size);

    /**
     * 商品打折查询
     *
     * @return 商品信息
     */
    List<ProductDiscountsParam> queryProductDiscount();

    /**
     * 商品数量查询
     *
     * @return 商品数量
     */
    List<Map<String, Map<String, Integer>>> queryCount();

    /**
     * 查询商品
     *
     * @return
     */
    List<Map<String, Map<String, List<ProductInfo>>>> merchandise();

    /**
     * 商品详情展示
     *
     * @param productId 商品id
     * @return 商品信息
     */
    ProductDetailsParam queryProductInfo(String productId);
}
