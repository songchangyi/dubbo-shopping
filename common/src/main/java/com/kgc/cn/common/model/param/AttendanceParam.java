package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by scy on 2019/12/18
 */

@Data
@ApiModel("打卡")
public class AttendanceParam implements Serializable {
    private static final long serialVersionUID = 6093547572331609996L;
    @ApiModelProperty("id")
    private String employeeId;
    @ApiModelProperty("姓名")
    private String employeeName;
    @ApiModelProperty("部门")
    private String departmentName;
    @ApiModelProperty("打卡时间")
    private Date time;
}
