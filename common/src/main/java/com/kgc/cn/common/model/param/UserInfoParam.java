package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户信息")
public class UserInfoParam implements Serializable {
    private static final long serialVersionUID = 8378785430970347201L;
    @ApiModelProperty(value = "昵称", required = true)
    private String nickname;
    @ApiModelProperty("手机号")
    private String userPhone;
    @ApiModelProperty("邮箱")
    private String userEmail;
    @ApiModelProperty("密码")
    private String userPassword;
    @ApiModelProperty(value = "年龄", example = "1")
    private Integer userAge;
    @ApiModelProperty(value = "性别", example = "1")
    private Integer userSex;


}