package com.kgc.cn.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRole implements Serializable {
    private static final long serialVersionUID = 8275559816124155429L;
    private Integer id;

    private String roleId;

    private String roleName;

    private String roleLevel;


}