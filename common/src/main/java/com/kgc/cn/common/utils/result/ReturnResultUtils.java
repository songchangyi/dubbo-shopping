package com.kgc.cn.common.utils.result;

import com.kgc.cn.common.enums.ReturnEnum;
import com.kgc.cn.common.utils.ReturnResult;

/***
 * created by 北大课工场
 *
 * 统一返回工具类
 */
public class ReturnResultUtils {

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage("success");
        return returnResult;
    }

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 成功 带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage(msg);
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }


    public static ReturnResult returnSuccess(ReturnEnum returnEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(returnEnum.getCode());
        returnResult.setMessage(returnEnum.getMessage());
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Object e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setData(e);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(ReturnEnum returnEnum) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(returnEnum.getCode());
        returnResult.setMessage(returnEnum.getMessage());
        return returnResult;
    }

    public static ReturnResult returnFail(ReturnEnum re, Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(re.getCode());
        returnResult.setMessage(re.getMessage());
        returnResult.setData(data);
        return returnResult;
    }
}
