package com.kgc.cn.common.utils.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public interface TimeUtils {
    static String getSystemTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return df.format(new Date());
    }

    static String getOrderTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(new Date());
    }

    static String getCurrentTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        return df.format(new Date());
    }

    static Date getNextDay(Date date, int day) {
        if (date == null) {
            System.out.println("入参为空");
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    static String getNextDayString(Date date, int day) {
        Date newDate = getNextDay(date, day);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(newDate);
    }
}
