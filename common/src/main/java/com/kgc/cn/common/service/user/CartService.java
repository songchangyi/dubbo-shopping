package com.kgc.cn.common.service.user;

import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.vo.ShoppingCartVo;

import java.util.List;

/**
 * Created by scy on 2019/12/23
 */
public interface CartService {


    /**
     * 购物车商品增加（登录时同步）
     *
     * @param shoppingCart 购物车信息
     * @return 影响条数
     */
    int addProductCartLogin(ShoppingCart shoppingCart);

    /**
     * 购物车商品详情展示（登录时）
     *
     * @param userId 用户id
     * @return 购物车商品信息
     */
    List<ShoppingCart> showProductCartLogin(String userId);

    /**
     * 购物车商品详情展示分页（登录时）
     *
     * @param userId 用户id
     * @param page   当前页码
     * @param size   每页条数
     * @return 购物车信息
     */
    PageInfo<ShoppingCart> showProductCartLogin(String userId, int page, int size);

    /**
     * 购物车商品删除（登录时）
     *
     * @param productId 商品id
     * @return 影响条数
     */
    int delProductCartLogin(String productId);

    /**
     * 通过商品id查询购物车信息
     *
     * @param productId 商品id
     * @return 购物车信息
     */
    ShoppingCartVo queryProductCart(String productId);

    /**
     * 清空购物车（登录时）
     *
     * @param userId 用户id
     * @return 影响条数
     */
    int delAllProductCartLogin(String userId);


    /**
     * 购物车商品数量减少（登录时）
     *
     * @param shoppingCart 购物车商品信息
     * @return 影响条数
     */
    int updateProductCartLogin(ShoppingCart shoppingCart);

    /**
     * 购物车商品数量增加（登录时）
     *
     * @param shoppingCart 购物车商品信息
     * @return 影响条数
     */
    int addCartLogin(ShoppingCart shoppingCart);


}
