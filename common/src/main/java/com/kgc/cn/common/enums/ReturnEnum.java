package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface ReturnEnum {


    int getCode();

    String getMessage();

    @Getter
    @AllArgsConstructor
    enum PhoneEunm implements ReturnEnum {
        NONE(-1, "手机号未注册"),
        REPEAT(1, "重复");

        private int code;
        private String message;

    }

    @Getter
    @AllArgsConstructor
    enum UserEnum implements ReturnEnum {
        NONE(-1, "用户未注册"),
        REPEAT(1, "用户已注册"),
        ERROR(10001, "用户名或密码错误"),
        MsgErr(10002, "登录信息异常"),
        PHMA(0, "请输入邮箱或手机号"),
        MAIL(3, "邮箱已注册"),
        PHONE(2, "手机已注册"),
        LOGIN(4, "用户已登录"),
        REGISTERSUCCESS(100, "注册成功"),
        LOGINSUCCESS(101, "登陆成功"),
        NEEDPHONE(102, "请绑定手机号"),
        YJZC(10003, "已经注册"),
        OPENID(10004, "绑定微信成功");
        private int code;
        private String message;
    }


    @AllArgsConstructor
    @Getter
    enum GoodEnum implements ReturnEnum {
        NUM_FAIL(1001, "添加超过库存"),
        NONE(7001, "无此商品"),
        REPEAT(7002, "商品已存在"),
        OVER(7003, "商品已售空"),
        UPDATE(7004, "修改成功！"),
        ADD_FAIL(1001, "添加失败,商品不存在"),
        DELFAIL(7005, "商品不存在");

        private int code;
        private String message;
    }


    @AllArgsConstructor
    @Getter
    enum ShoppingCarEnum implements ReturnEnum {
        DEL_ALL_FAIL(1002, "购物车已经空了"),
        DEL_FAIL(1001, "没有此物品"),
        FAIL(9001, "添加失败");

        private int code;
        private String message;
    }


    @AllArgsConstructor
    @Getter
    enum OrderEnum implements ReturnEnum {
        ERROR(8001, "订单不存在或已完成"),
        PAYFAIL(8010, "支付失败");

        private int code;
        private String message;
    }

    @Getter
    @AllArgsConstructor
    enum EmployeeEnum implements ReturnEnum {

        NONE(-1, "无此员工"),
        UPDATE_NONE(100002, "数据不能为空！"),
        FAIL(10001, "权限不足"),
        ERROR(10003, "员工编号不能为空");


        private int code;
        private String message;
    }


    @AllArgsConstructor
    @Getter
    enum EmployeeAccountEnum implements ReturnEnum {

        PUNCH_CARD_FAIL(10004, "对不起，您今天已打过卡"),

        LOGIN_FAIL(10003, "账号未登录"),

        NONE(100001, "您已离职，无法登录"),

        ERROR(-1, "账号或者密码错误"),

        DEL_FAIL(10002, "删除失败,账号已注销"),

        ROLE_FAIL(-1, "删除失败，您的权限不够！"),

        OLOGIN(2, "账号已经登录");


        private int code;
        private String message;
    }


    @AllArgsConstructor
    @Getter
    enum ExcelEnum implements ReturnEnum {

        SHEET_NAME(-1001, "表名错误！");

        private int code;
        private String message;
    }

    @AllArgsConstructor
    @Getter
    enum CartEnum implements ReturnEnum {

        DEL_FAIL(10001, "删除失败"),
        NONE(-1, "没有东西,是空的");

        private int code;
        private String message;
    }

    @AllArgsConstructor
    @Getter
    enum CodeEnum implements ReturnEnum {

        CODE_ERROR(50001, "验证码错误");

        private int code;
        private String message;
    }
}
