package com.kgc.cn.common.model.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by boot on 2019/12/23.
 */
@Data
@ApiModel(value = "商品详情展示")
public class ProductDetailsParam implements Serializable {

    private static final long serialVersionUID = -8572195766818041366L;

    @ApiModelProperty(value = "商品id")
    private String productId;
    @ApiModelProperty(value = "商品名称")
    private String productName;
    @ApiModelProperty(value = "商品种类")
    private String categoryId;
    @ApiModelProperty(value = "商品原价", example = "1.00")
    private BigDecimal originalPrice;
    @ApiModelProperty(value = "折扣", example = "1")
    private Integer discountLevel;
    @ApiModelProperty(value = "折扣价", example = "1.00")
    private BigDecimal discountPrice;

}
