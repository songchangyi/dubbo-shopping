package com.kgc.cn.common.model.param.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by scy on 2019/12/10
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailParam implements Serializable {

    private static final long serialVersionUID = -5710252666619271173L;
    private String to;
    private String subject;
    private String content;

    private Map<String, String> pictures;

    String[] filePaths;

    Map<String, Object> templateParams;


}
