package com.kgc.cn.mapper;

import com.kgc.cn.common.model.dto.VipRecord;
import com.kgc.cn.common.model.dto.example.VipRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VipRecordMapper {
    long countByExample(VipRecordExample example);

    int deleteByExample(VipRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VipRecord record);

    int insertSelective(VipRecord record);

    List<VipRecord> selectByExample(VipRecordExample example);

    VipRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VipRecord record, @Param("example") VipRecordExample example);

    int updateByExample(@Param("record") VipRecord record, @Param("example") VipRecordExample example);

    int updateByPrimaryKeySelective(VipRecord record);

    int updateByPrimaryKey(VipRecord record);
}