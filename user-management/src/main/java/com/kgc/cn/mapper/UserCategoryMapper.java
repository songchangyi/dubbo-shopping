package com.kgc.cn.mapper;

import com.kgc.cn.common.model.dto.UserCategory;
import com.kgc.cn.common.model.dto.example.UserCategoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserCategoryMapper {
    long countByExample(UserCategoryExample example);

    int deleteByExample(UserCategoryExample example);

    int deleteByPrimaryKey(String categoryId);

    int insert(UserCategory record);

    int insertSelective(UserCategory record);

    List<UserCategory> selectByExample(UserCategoryExample example);

    UserCategory selectByPrimaryKey(String categoryId);

    int updateByExampleSelective(@Param("record") UserCategory record, @Param("example") UserCategoryExample example);

    int updateByExample(@Param("record") UserCategory record, @Param("example") UserCategoryExample example);

    int updateByPrimaryKeySelective(UserCategory record);

    int updateByPrimaryKey(UserCategory record);
}