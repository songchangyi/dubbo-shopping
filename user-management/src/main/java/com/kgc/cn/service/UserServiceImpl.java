package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.UserInfo;
import com.kgc.cn.common.model.dto.VipRecord;
import com.kgc.cn.common.model.vo.UserInfoVo;
import com.kgc.cn.common.service.user.UserService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.mapper.UserInfoMapper;
import com.kgc.cn.mapper.VipRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    VipRecordMapper vipRecordMapper;

    @Override
    public int registered(UserInfoVo userInfoVo) {
        UserInfo userInfo = CopyUtils.copy(userInfoVo, UserInfo.class);
        userInfo.setCategoryId("10001");
        userInfo.setDeleteFlag(0);
        return userInfoMapper.insertSelective(userInfo);
    }

    @Override
    public List<UserInfoVo> select(UserInfoVo userInfoVo) {
        UserInfo userInfo = CopyUtils.copy(userInfoVo, UserInfo.class);
        return CopyUtils.copyList(userInfoMapper.select(userInfo), UserInfoVo.class);
    }

    @Override
    public UserInfoVo phoneLogin(String userPhone, String userPassword) {
        UserInfo userInfo = userInfoMapper.phoneLogin(userPhone, userPassword);
        return Objects.isNull(userInfo) ? null : CopyUtils.copy(userInfo, UserInfoVo.class);
    }

    @Override
    public UserInfoVo mailLogin(String userEmail, String userPassword) {
        UserInfo userInfo = userInfoMapper.mailLogin(userEmail, userPassword);
        return Objects.isNull(userInfo) ? null : CopyUtils.copy(userInfo, UserInfoVo.class);
    }

    @Override
    public int addVipRecord(VipRecord record) {
        return vipRecordMapper.insertSelective(record);
    }

    @Override
    public int updateUser(UserInfoVo userInfoVo) {
        UserInfoVo userInfo = userInfoVo;
        return userInfoMapper.updateByPrimaryKey(CopyUtils.copy(userInfoVo, UserInfo.class));
    }


}
