package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.OrderDetail;
import com.kgc.cn.common.model.dto.ShoppingCart;
import com.kgc.cn.common.model.dto.example.OrderDetailExample;
import com.kgc.cn.common.model.vo.OrderDetailVo;
import com.kgc.cn.common.service.user.CartService;
import com.kgc.cn.common.service.user.OrderService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.mapper.OrderDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by scy on 2019/12/23
 */

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Reference
    private CartService cartService;

    @Autowired
    OrderDetailMapper detailMapper;

    @Override
    public int addVipOrder(String userId) {
        List<ShoppingCart> shoppingCartList = cartService.showProductCartLogin(userId);
        shoppingCartList.forEach(shoppingCarts -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setUserId(userId);
            orderDetail.setProductId(shoppingCarts.getProductId());
            orderDetail.setProductNum(shoppingCarts.getProductNum());
            orderDetailMapper.insert(orderDetail);
        });
        return 0;
    }


    @Override
    public int addOrder(OrderDetail orderDetail) {
        return detailMapper.insertSelective(orderDetail);
    }

    @Override
    public OrderDetailVo selectOrder(String orderId, String userId) {
        OrderDetailExample orderDetailExample = new OrderDetailExample();
        orderDetailExample.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        List<OrderDetail> orderDetails = orderDetailMapper.selectByExample(orderDetailExample);
        if (CollectionUtils.isEmpty(orderDetails)) {
            return null;
        }
        OrderDetailVo orderDetailVo = CopyUtils.copy(orderDetails.get(0), OrderDetailVo.class);
        return orderDetailVo;
    }

    @Override
    public int changeState(String orderId, Integer state) {
        OrderDetailExample orderDetailExample = new OrderDetailExample();
        orderDetailExample.createCriteria().andOrderIdEqualTo(orderId);
        return detailMapper.updateByExampleSelective(OrderDetail.builder().orderState(state).build(), orderDetailExample);
    }
}
