package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.dto.example.EmployeeAccountExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeAccountMapper {
    long countByExample(EmployeeAccountExample example);

    int deleteByExample(EmployeeAccountExample example);

    int deleteByPrimaryKey(String employeeId);

    int insert(EmployeeAccount record);

    int insertSelective(EmployeeAccount record);

    List<EmployeeAccount> selectByExample(EmployeeAccountExample example);

    EmployeeAccount selectByPrimaryKey(String employeeId);

    int updateByExampleSelective(@Param("record") EmployeeAccount record, @Param("example") EmployeeAccountExample example);

    int updateByExample(@Param("record") EmployeeAccount record, @Param("example") EmployeeAccountExample example);

    int updateByPrimaryKeySelective(EmployeeAccount record);

    int updateByPrimaryKey(EmployeeAccount record);
}