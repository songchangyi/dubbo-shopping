package com.kgc.cn.provider.config.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.kgc.cn.provider.mapper")
public class MybatisConfig {
}
