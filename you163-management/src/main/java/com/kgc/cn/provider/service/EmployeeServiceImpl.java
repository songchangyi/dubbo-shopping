package com.kgc.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.dto.EmployeeAttendance;
import com.kgc.cn.common.model.dto.EmployeeInfo;
import com.kgc.cn.common.model.dto.example.EmployeeInfoExample;
import com.kgc.cn.common.model.param.AttendanceParam;
import com.kgc.cn.common.model.param.EmployeeCheckOut;
import com.kgc.cn.common.model.param.EmployeeParam;
import com.kgc.cn.common.model.vo.EmployeeInfoVo;
import com.kgc.cn.common.service.management.EmployeeService;
import com.kgc.cn.common.utils.copy.CopyUtils;
import com.kgc.cn.common.utils.encrypt.EncryptUtil;
import com.kgc.cn.provider.mapper.EmployeeAccountMapper;
import com.kgc.cn.provider.mapper.EmployeeAttendanceMapper;
import com.kgc.cn.provider.mapper.EmployeeInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by scy on 2019/12/10
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeInfoMapper employeeInfoMapper;

    @Autowired
    EmployeeAccountMapper employeeAccountMapper;

    @Autowired
    EmployeeAttendanceMapper attendanceMapper;


    @Override
    public int addEmployee(EmployeeInfoVo employeeInfoVo) {
        EmployeeInfo employeeInfo = CopyUtils.copy(employeeInfoVo, EmployeeInfo.class);
        employeeInfo.setIsDimission(0);
        return employeeInfoMapper.insertSelective(employeeInfo);

    }


    @Override
    public EmployeeInfoVo toLogin(String phone, String password) {
        EmployeeInfo employeeInfo = employeeInfoMapper.toLogin(phone, EncryptUtil.getInstance().Base64Encode(password));
        // 判断获取的员工信息是否为空
        if (Objects.isNull(employeeInfo)) {
            return null;
        }
        return CopyUtils.copy(employeeInfo, EmployeeInfoVo.class);
    }


    @Override
    public int addEmployeeAccount(EmployeeAccount employeeAccount) {
        employeeAccount.setPassword(EncryptUtil.getInstance().Base64Encode(employeeAccount.getPassword()));
        return employeeAccountMapper.insertSelective(employeeAccount);
    }


    @Override
    public int delEmployee(String employeeId) {
        EmployeeInfoExample employeeInfoExample = new EmployeeInfoExample();
        employeeInfoExample.createCriteria().andEmployeeIdEqualTo(employeeId);
        EmployeeInfo employeeInfo = EmployeeInfo.builder().isDimission(1).dimissionTime(new Date()).build();
        return employeeInfoMapper.updateByExampleSelective(employeeInfo, employeeInfoExample);
    }


    @Override
    public List<EmployeeInfoVo> queryEmployee(EmployeeParam employeeParam) {
        List<EmployeeInfo> employeeInfoList = employeeInfoMapper.queryEmployee(employeeParam);
        List<EmployeeInfoVo> employeeInfoVoList = Lists.newArrayList();
        employeeInfoList.forEach(employeeInfoVos -> {
            EmployeeInfoVo employeeInfoVo = CopyUtils.copy(employeeInfoVos, EmployeeInfoVo.class);
            employeeInfoVoList.add(employeeInfoVo);
        });
        return employeeInfoVoList;

    }


    @Override
    public EmployeeInfoVo queryEmployeeInfo(String employeeId) {
        EmployeeInfo employeeInfo = employeeInfoMapper.queryEmployeeInfo(employeeId);
        EmployeeInfoVo employeeInfoVo = new EmployeeInfoVo();
        BeanUtils.copyProperties(employeeInfo, employeeInfoVo);
        return employeeInfoVo;
    }


    @Override
    public int updateEmployeeInfo(EmployeeInfoVo employeeInfoVo) {
        EmployeeInfo employeeInfo = CopyUtils.copy(employeeInfoVo, EmployeeInfo.class);
        return employeeInfoMapper.updateByPrimaryKeySelective(employeeInfo);
    }

    @Override
    public List<EmployeeCheckOut> checkOut() {
        return employeeInfoMapper.checkOut();
    }


    @Override
    public int updateEmployeeAccount(EmployeeAccount employeeAccount) {
        return employeeAccountMapper.updateByPrimaryKeySelective(employeeAccount);
    }

    @Override
    public PageInfo<EmployeeInfoVo> paging(EmployeeParam employeeParam, int page, int size) {
        PageInfo<EmployeeInfoVo> infoVoPageInfo = PageHelper.startPage(page, size).doSelectPageInfo(() -> queryEmployee(employeeParam));
        return infoVoPageInfo;
    }

    @Override
    @Transactional
    public PageInfo<AttendanceParam> queryAttendance(boolean flag, int page, int size) {
        if (flag) {
            return PageHelper.startPage(page, size).doSelectPageInfo(() -> attendanceMapper.attendance());

        }
        return PageHelper.startPage(page, size).doSelectPageInfo(() -> attendanceMapper.disAttendance());
    }


    @Override
    public int addEmployeeAttendance(String employeeId) {
        EmployeeAttendance employeeAttendance = EmployeeAttendance.builder().employeeId(employeeId).build();
        return attendanceMapper.insertSelective(employeeAttendance);
    }
}
