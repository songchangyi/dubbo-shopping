package com.kgc.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.model.dto.EmployeeAccount;
import com.kgc.cn.common.model.dto.EmployeeRole;
import com.kgc.cn.common.model.dto.example.EmployeeAccountExample;
import com.kgc.cn.common.model.dto.example.EmployeeRoleExample;
import com.kgc.cn.common.service.management.PowerService;
import com.kgc.cn.provider.mapper.EmployeeAccountMapper;
import com.kgc.cn.provider.mapper.EmployeeRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by scy on 2019/12/13
 */

@Service
public class PowerServiceImpl implements PowerService {

    @Autowired
    EmployeeRoleMapper roleMapper;

    @Autowired
    EmployeeAccountMapper accountMapper;


    @Override
    public String showRoleLevel(String roleId) {
        EmployeeRoleExample employeeRoleExample = new EmployeeRoleExample();
        employeeRoleExample.createCriteria().andRoleIdEqualTo(roleId);
        List<EmployeeRole> employeeRoles = roleMapper.selectByExample(employeeRoleExample);
        if (CollectionUtils.isEmpty(employeeRoles)) {
            return null;
        }
        return employeeRoles.get(0).getRoleLevel();
    }

    @Override
    public String showAccountLevel(String employeeId) {
        EmployeeAccountExample accountExample = new EmployeeAccountExample();
        accountExample.createCriteria().andEmployeeIdEqualTo(employeeId);
        List<EmployeeAccount> accounts = accountMapper.selectByExample(accountExample);
        if (CollectionUtils.isEmpty(accounts)) {
            return null;
        }
        return accounts.get(0).getLevel();
    }
}
